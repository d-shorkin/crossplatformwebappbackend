var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var fileUpload = require('express-fileupload');

var passport = require('passport');


process.env["NODE_CONFIG_DIR"] = __dirname + "/config/";
var config = require('config');

var mongoose = require('mongoose');
mongoose.connect(config.get("Main.mongoDbConfig"));
mongoose.Promise = Promise;

if(typeof NODE_ENV == "undefined" || NODE_ENV != "production")
{
    mongoose.set('debug', true);
}

var cors = require('cors');

var app = express();

app.use(cors());

app.use(passport.initialize());

app.use(fileUpload({
    limits: { fileSize: 50 * 1024 * 1024 },
}));

require('./app/passport')(passport);

// Routes
var index = require('./routes/index');
var api = require('./routes/api')(passport);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
//app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
app.use('/api', api);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
