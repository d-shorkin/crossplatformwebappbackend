const socketioJwt = require('socketio-jwt');
const config = require('config');

const Message = require("./models/message");
const User = require("./models/user");
const Dialog = require("./models/dialog");

module.exports = function (server, port) {
    let io = require('socket.io')(server);

    return io
        .on('connection', socketioJwt.authorize({
            secret: config.get("Main.tokenSecret"),
            timeout: 15000
        }))
        .on('authenticated', function (socket) {
            let userId = socket.decoded_token._id;

            socket.join(userId);

            User.findOne({_id: socket.decoded_token._id}).then((user=>{
                socket.emit('user', user);
            }));

            socket.on('search', (email)=>{
                User.find({$or: [{email: new RegExp('.*'+email+'.*', "i")}, {username: new RegExp('.*'+email+'.*', "i")}] , _id: {$ne: userId}})
                    .then(users => { socket.emit("users", users) });
            });

            socket.on('getHistory', (dialog_id)=>{
                Message
                    .find({dialog: dialog_id})
                    .then(messages=>{
                        socket.emit("history", messages);
                    })

            });

            socket.on('message', (data) => {

                Dialog
                    .find({users: {$all: [userId, data.to_id]}})
                    .then((dialogs)=>{
                        if(!dialogs.length){
                            let dialog = new Dialog({ users: [userId, data.to_id] });
                            return dialog.save()
                        }

                        return dialogs[0];
                    })
                    .then(dialog=>{
                        let message = new Message({
                            from: userId,
                            dialog: dialog._id,
                            message: data.message,
                            created_at: Date.now()
                        });

                        return message.save();
                    })
                    .then(message => {

                        console.log(`New message [${message.dialog}]: "${message.message}"`);
                        return Dialog.findOneAndUpdate({_id: message.dialog}, {lastMessage: message._id});
                    })
                    .then(()=>{
                        io.sockets.in(data.to_id).emit('updates');

                        // update user dialogs
                        Dialog
                            .find({users: data.to_id})
                            .populate('users')
                            .populate('lastMessage')
                            .then(dialogs => io.sockets.in(data.to_id).emit("dialogs", dialogs));

                        // update user dialogs
                        Dialog
                            .find({users: userId})
                            .populate('users')
                            .populate('lastMessage')
                            .then(dialogs => socket.emit("dialogs", dialogs));
                    });
            });

            socket.on('change-username', (username)=>{
                User.findOneAndUpdate({_id: socket.decoded_token._id}, {$set:{username}}, function(err, doc){
                    socket.emit('user', Object.assign(doc, {username}));
                });
            });

            // update user dialogs
            Dialog
                .find({users: userId})
                .populate('users')
                .populate('lastMessage')
                .then(dialogs => socket.emit("dialogs", dialogs));
        });
};