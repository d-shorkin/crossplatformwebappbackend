let mongoose = require('mongoose');

let ObjectId = mongoose.Schema.ObjectId;

let schema = new mongoose.Schema({
    users:[{
        type: ObjectId,
        required: true,
        ref: 'User'
    }],
    lastMessage: {
        type: ObjectId,
        ref: 'Message'
    }
});


module.exports = mongoose.model('Dialog', schema);