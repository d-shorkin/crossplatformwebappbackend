let mongoose = require('mongoose');

let ObjectId = mongoose.Schema.ObjectId;

let schema = new mongoose.Schema({
    from:{
        type: ObjectId,
        required: true,
        ref: 'User'
    },
    dialog:{
        type: ObjectId,
        required: true,
        ref: 'Dialog'
    },
    message: {
        type: String,
        required: true
    },
    created_at: {
        type: Date,
        default: Date.now
    }
});


module.exports = mongoose.model('Message', schema);