// Create API group routes
var express = require('express');
var router = express.Router();

const User = require("./../app/models/user");

var jwt    = require('jsonwebtoken');
var config = require('config');

var path = require('path');


function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

module.exports = function (passport) {

// Register new users
    router.post('/register', function(req, res) {
        if(!req.body.email || !req.body.password) {
            res.json({ success: false, message: 'Please enter email and password.' });
        } else {
            var newUser = new User({
                email: req.body.email,
                password: req.body.password,
                username: req.body.email
            });

            // Attempt to save the user
            newUser.save(function(err) {
                if (err) {
                    return res.json({ success: false, message: 'That email address already exists.'});
                }
                res.json({ success: true, message: 'Successfully created new user.' });
            });
        }
    });

    router.post('/authenticate', function(req, res) {
        User.findOne({
            email: req.body.email
        }, function(err, user) {
            if (err) throw err;

            if (!user) {
                res.send({ success: false, message: 'Authentication failed. User not found.' });
            } else {
                // Check if password matches
                user.comparePassword(req.body.password, function(err, isMatch) {
                    if (isMatch && !err) {
                        // Create token if the password matched and no error was thrown
                        var token = jwt.sign(user.toJSON(), config.get("Main.tokenSecret"), {
                            expiresIn: 10080 // in seconds
                        });
                        res.json({ success: true, token: token });
                    } else {
                        res.send({ success: false, message: 'Authentication failed. Passwords did not match.' });
                    }
                });
            }
        });
    });

    // Protect dashboard route with JWT
    router.get('/dashboard', passport.authenticate('jwt', { session: false }), function(req, res) {
        res.send('It worked! User id is: ' + req.user._id + '.');
    });

    router.post('/upload', passport.authenticate('jwt', { session: false }), function(req, res) {
        console.log('User: ' + req.user._id + ' - Upload new image.');

        if (!req.files)
            return res.status(400).json({ success: false, message: 'No files were uploaded.' });

        let ext = path.extname(req.files.file.name);

        let salt = getRandomInt(10, 99);

        let imgPath = __dirname + '/../public/upload/' + req.user._id + salt + ext;

        // Use the mv() method to place the file somewhere on your server
        req.files.file.mv(imgPath, function(err) {
            if (err)
                return res.status(500).json({ success: false, message: err.message });

            let url = 'upload/' + req.user._id + salt + ext;

            User.findOneAndUpdate({_id: req.user._id}, {$set:{avatar:url}}, function(err, doc){
                if(err)
                    return res.status(500).json({ success: false, message: err.message });

                res.json({ success: true, message: 'File uploaded!', url});
            });




        });
    });

    return router;
};
